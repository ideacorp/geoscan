// JavaScript Document
(function($) {
    "use strict";
	
	//calling foundation js
	$(document).foundation();
	
	//Display Scroll Btn on 1000px
	$(window).on("scroll",function() { 
		if($(this).scrollTop() > 1000) { 
			$("#top").fadeIn();
		} else { 
			$("#top").fadeOut();
		}
	});
	
	//scroll effect
	$("#top").on("click",function () {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});        
	
	$("#top").on("click",function (event) {
		event.stopPropagation();                
		var idTo = $(this).attr("data-atr");                
		var Position = $("[id='" + idTo + "']").offset();
		$("html, body").animate({ scrollTop: Position }, "slow");
		return false;
	});

	//Animation effect on Gallery
	$(".project").on("mouseenter",function() {
		var animationEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
		$(this).children(".project-detail").addClass("animated fadeIn").on(animationEnd, function() {
		$(this).removeClass("animated fadeIn");
		});
	});
	
	//calling Brand Crousel
	$('.main-banner').owlCarousel({
		loop:true,
		margin:0,
		autoplay:true,
	    autoplayTimeout:10000,
    	autoplayHoverPause:false,
		responsiveClass:true,
		dots:true,
		nav: true,
		responsive:{
			0:{
				items:1,
				nav:true, 			
			},
			600:{
				items:1,
				nav:true, 			
			},
			1000:{
				items:1,
				nav:true, 			
			}
		}
	});
	var newProducts = $('.main-banner');
	newProducts.find('.owl-prev').html('<i class="fa fa-angle-left"></i>');
	newProducts.find('.owl-next').html('<i class="fa fa-angle-right"></i>');

	var owl = $('.partner-banner');
	owl.owlCarousel({
		items:4,
		loop:true,
		margin:10,
		autoplay:true,
		autoplayTimeout:10000,
		autoplayHoverPause:true,
		dots: true,
		responsive:{
			0:{
				items:1,
				nav:false, 			
			},
			600:{
				items:4,
				nav:false, 			
			},
			1000:{
				items:4,
				nav:false, 			
			}
		}
	});

	
	//calling Brand Crousel
	$('.testimonials-box').owlCarousel({
		loop:true,
		margin:0,
		autoplay:true,
	    autoplayTimeout:3000,
    	autoplayHoverPause:true,
		responsiveClass:true,
		dots:true,
		responsive:{
			0:{
				items:1,
				nav:true, 			
			},
			600:{
				items:1,
				nav:false, 			
			},
			1000:{
				items:1,
				nav:false, 			
			}
		}
	});
	
})(jQuery); //jQuery main function ends strict Mode on 

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-62711679-1', 'auto');
  ga('send', 'pageview');
  
  
  var isNS = (navigator.appName == "Netscape") ? 1 : 0;

if(navigator.appName == "Netscape") document.captureEvents(Event.MOUSEDOWN||Event.MOUSEUP);

function mischandler(){
return false;
}




document.onkeydown = function(e) {
        if (e.ctrlKey && 
            (e.keyCode === 67 || 
             e.keyCode === 86 || 
             e.keyCode === 85 || 
             e.keyCode === 117)) {
            alert('not allowed');
            return false;
        } else {
            return true;
        }
};


function initMap() {
    if ($('.google-map').length) {
        var locations = [
            ['GeoScan', -3.741269,-38.513813, 1]
		];
		
        var map = new google.maps.Map(document.getElementById('gmap_contact'), {
            zoom: 14,
            center: new google.maps.LatLng(-3.741269,-38.513813),
                        scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    };
}