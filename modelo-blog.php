<?php 

get_header();

/* Template Name: Blog */

?>
        
        <div class="title-section module">
            <div class="row">
        
                <div class="small-12 columns">
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
        
                <div class="small-12 columns">
                    <ul class="breadcrumbs">
                        <li><a href="#">Home</a></li>
                        <li><span class="show-for-sr"><?php echo get_the_title(); ?></li>
                    </ul>
                </div>
                
            </div>
        </div>
         

 	    <div class="single-service module">
			<div class="row">
				
				<div class="medium-9 small-12 columns">
					
					<div class="row padding-between">
                    
                    <?php

                        $args = array(
                            'posts_per_page'    => 12,
                            'order'          => 'ASC',
                        );

                        $the_query = new WP_Query( $args );

                        if ( $the_query->have_posts() )
                        {
                            while ( $the_query->have_posts() )
                            {
                                $the_query->the_post();
                    ?>
                    
                                    <div class="medium-12 small-12 columns">
                                        <div class="news">
                                            <a href="<?php echo get_the_permalink(); ?>">
                                                <img src="<?php echo wp_get_attachment_image_src(get_field('imagem'),'imagem-sobre')[0]; ?>" alt="<?php echo get_the_title(); ?>" class="thumbnail" />
                                            </a>
                                            <div class="news-text">
                                                <h2><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
                                                <p><?php echo get_field('paragrafo'); ?><a href="<?php echo get_the_permalink(); ?>">Leia Mais →</a></p>
                                            </div>
                                        </div>
                                    </div>
                                                
                    <?php
                            }
                            wp_reset_postdata();
                        }
                        else
                        {
                            echo "Sem descontos cadastrados";
                        }
                    ?>
						
					</div>
		            
				</div>

				<?php get_sidebar(); ?>
				
			</div>
        </div>
        
<?php get_footer(); ?>