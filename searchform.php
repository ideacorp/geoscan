<div class="widget search">
    <div class="widget-content">
        <form action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <input type="text" placeholder="Procure uma postagem..." />
            <button class="button primary"><i class="fa fa-search"></i></button>
        </form>
    </div>
</div>