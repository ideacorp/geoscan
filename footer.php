 
        
		<div class="call-to-action">
			<div class="row">
				
				<div class="medium-12 small-12 columns">
					<div class="float-left">
                            
						<h2>Entre em contato conosco!</span></h2>
						<p>Fale com a gente através de nosso telefone, teremos o maior prazer em lhe atender.</p>
					</div>
					<div class="float-right call-us">
						<div class="float-left callus-icon"  style="color: white;">
							<i class="fa fa-volume-control-phone"></i>
						</div>
						<div class="float-right text-center"  >
							<h2 style="color: white;">Ligue agora!</h2>
							<p style="color: white;">(85) 4101-9199</p>
						</div>
					</div>
					<div class="clearfix"></div>	
				</div>
				
			</div>
		</div>
        
        
        
        <div class="footer">
        	
            <div class="footer-top">
            	<div class="row">
        	
            		<div class="medium-3 small-12 columns">
                        <div class="footer-box">
                            <img src="<?php echo get_template_directory_uri()?>/assets/images/logo-footer.png" alt=""/>
                            <ul>
                                <li><i class="fa fa-map-marker"></i><?php echo get_field('dados_de_contato', 'option')['endereco'] ?></li>     
                                <li><i class="fa fa-phone"></i><?php echo get_field('dados_de_contato', 'option')['telefone'] ?></li>
                            </ul>
                        </div>
            		</div>
                
            		<div class="medium-3 small-12 columns">
                        <div class="footer-box border-btm">
                            <h2>Links Utéis</h2>
                            <ul>
                            <?php
                            
                                $localizacao = get_nav_menu_locations();
                                $menu = wp_get_nav_menu_object( $localizacao[ 'menu-footer' ] );
                                $menuitems = wp_get_nav_menu_items( $menu->term_id);
                                
                                $aberto = False;
                                $saida = "";
                                $contador = 0;

                                foreach( $menuitems as $item )
                                {
                                    $link = $item->url; 
                                    $titulo = $item->title;
                                    $class = $item->classes[0];

                                    if ( $item->menu_item_parent == 0 )
                                    {
                                        if ($menuitems[$contador+1]->menu_item_parent == 0)
                                        {
                                            $saida .= ' <li>
                                                            <a href="'.$link.'">'.$titulo.'</a> 
                                                        </li>';
                                        }
                                    }
                                    
                                    $contador++;
                                }
                                echo $saida;

                            ?> 
                            </ul>
                        </div>
                	</div>
                
                	<div class="medium-3 small-12 columns end">
                        <div class="footer-box border-btm">
                            <h2>Serviços</h2>
                            <ul>

                            <?php

                                $args = array(
                                    'posts_per_page'    => 6,
                                    'post_type'         => 'servicos',
                                    'order'          => 'ASC',
                                );

                                $the_query = new WP_Query( $args );

                                if ( $the_query->have_posts() )
                                {
                                    while ( $the_query->have_posts() )
                                    {
                                        $the_query->the_post();
                            ?>
                            
                                <li><a href="<?php echo get_the_permalink(); ?>"><?php echo get_field('titulo_curto'); ?></a></li>                
                                                        
                            <?php
                                    }
                                    wp_reset_postdata();
                                }
                                else
                                {
                                    echo "Sem descontos cadastrados";
                                }
                            ?>

                            </ul>
                        </div>
                	</div>
                
        		</div>
        	</div>
            
            <div class="footer-bottom">
            	<div class="row">
                
                    <div class="medium-6 small-12 columns">
                        <div class="copyrightinfo"><small>GeoScan <i class="fa fa-code"></i> com <i class="fa fa-heart text-flatboots-red"></i> e <i class="fa fa-coffee"></i> por <a href="https://www.ideacorp.com.br" target="_blank"> Ideacorp</a> </small> </p></div>
                    </div>
                
                    <div class="medium-6 small-12 columns">
                        <div class="pull-right">
                            <ul class="menu">
                                <li><a href="<?php echo get_field('dados_de_contato', 'option')['facebook'] ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="<?php echo get_field('dados_de_contato', 'option')['instagram'] ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="<?php echo get_field('dados_de_contato', 'option')['linkedin'] ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
  
    <a href="#" id="top" title="Go to Top">
    	<i class="fa fa-long-arrow-up"></i>
    </a>
    
    <div class="preloader" id="loaderpre">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
	</div>
        
    
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri()?>/assets/images/favicon.png">

    
    
    <style>
    .flex-direction-nav a{
        height:50px;
        padding-top:10px;
    }
    </style>
   
    <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/assets/js/jquery.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/assets/js/jquery.flexslider.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/assets/js/foundation.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/assets/js/lightbox.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/style.css" media="all" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/css/flexslider.min.css" />
    <script src="<?php echo get_template_directory_uri()?>/assets/js/template.js"></script> 
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_bXG-q5B71txCRE36VnP07Q3r8H_VPVE&libraries=geometry&callback=initMap"></script>
    <?php
    if ( is_singular( 'servicos' ) ) {
        ?>

        <script type="text/javascript">
                
            $(window).load(function(){
                console.log('to aqui');
                $('#carousel').flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: false,
                    slideshow: false,
                    itemWidth: 210,
                    itemMargin: 5,
                    asNavFor: '#slider'
                });

                    
                $('#slider').flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: false,
                    slideshow: false,
                    sync: "#carousel",
                    start: function(slider){
                    $('body').removeClass('loading');
                    }
                });


            });
        </script>

    <?php }; ?>

   

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108368608-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-108368608-1');
    </script>

    <?php wp_footer(); ?>
</body>
</html>    