<?php 

/* Template Name: Sobre Nós */

get_header();

?>
        
        <div class="title-section module">
            <div class="row">
        
                <div class="small-12 columns">
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
        
                <div class="small-12 columns">
                    <ul class="breadcrumbs">
                        <li><a href="#">Home</a></li>
                        <li><span class="show-for-sr">Atual: </span> <?php echo get_the_title(); ?></li>
                    </ul>
                </div>
                
            </div>
        </div>
        
        <div class="about-section module">
            <div class="about-section-top">
                <div class="row">
                    
                    <div class="small-12 medium-6 columns section-top">
                        <div class="description-side">
                            <?php echo apply_filters('the_content', get_post_field('post_content', get_the_id())); ?>
                        </div>
                    </div>
                    
                    <div class="medium-6 small-12 columns about-image">
                    	<img style="border: 0px solid #CCC;" src="<?php echo wp_get_attachment_image_src(get_field('imagem'),'descontos-pequeno')[0]; ?>" alt="<?php get_the_title(); ?>" />
                    </div>
                                
                </div>
            </div>
        </div>
        
        <div class="about-section-bottom module">
            <div class="row">
            
            <?php

                if( have_rows('diferenciais') ):

                    while ( have_rows('diferenciais') ) : the_row();
                        
                        ?>

                            <div class="medium-4 small-12 columns">
                                <div class="section-inner">
                                    <div class="section-icon">
                                        <i class="fa <?php echo get_sub_field('icone'); ?>" aria-hidden="true"></i>
                                    </div>
                                    <div class="section-tex">
                                        <h2><?php echo get_sub_field('titulo'); ?></h2>
                                        <p><?php echo get_sub_field('texto'); ?></p>
                                    </div>
                                </div>
                            </div>

                        <?php
                    
                        
                    endwhile;

                else :

                    echo 'Sem diferenciais cadastrados';

                endif;

                ?>
                
            </div>
        </div>
         
         <div class="why-choose-us module" style="background:url('<?php echo wp_get_attachment_image_src(get_field('imagem_valores'),'descontos-pequeno')[0]; ?>'); background-size: contain; background-position: center;">
            <div class="why-choose-us-border">
            	
            	<div class="section-title small-module">
            		<h2>Os nossos<span> principais valores?</span></h2>
                	<p>Conheça os principais motivos que movem a nossa empresa.</p>
            	</div>
            	
            	<div class="row">
                	<div class="small-12 medium-6 columns">
                	
                    <?php

                        $contador = 1;
                        if( have_rows('valores') ):

                            while ( have_rows('valores') ) : the_row();
                                
                                ?>

                                    <div class="why-us-wrapper">
                                        <h1><?php echo $contador; ?></h1>
                                        <h2><?php echo get_sub_field('titulo'); ?></h2>
                                        <p><?php echo get_sub_field('texto'); ?></p>
                                        <?php $contador++; ?>
                                    </div>

                                <?php
                        
                            endwhile;

                        else :

                            echo 'Sem diferenciais cadastrados';

                        endif;

                    ?>
                    
                	</div>
            
            	</div>
            </div>
        </div>    
        
     <div class="our-brands module">
        
        <div class="section-title">
            <h2>Nossos <span>Parceiros e Clientes</span></h2>
            <p>Confira abaixo as parcerias que fechamos ao longo de toda nossa história.</p>                
        </div>
        
       
            <div class="partner-banner module">

            
                        <?php                             
                            if( have_rows('parceiros_e_clientes',get_page_by_path( 'sobre' )) ):
                                
                                while ( have_rows('parceiros_e_clientes',get_page_by_path( 'sobre' )) ) : the_row();

                                ?>
                            
                            <div class="brand-logo" style="padding: 25px; display: inline-block; width: 100%; height:160px">
                                <a href="<?php echo get_sub_field('link')  ?> " target="blank">
                                <img src="<?php echo wp_get_attachment_image_src(get_sub_field('imagem'),'imagem-parceiros')[0]; ?>" style="width:90%; display:inline"/> 
                                </a>
                            </div>

                           <?php

                                endwhile;
                            else :
                            
                                echo "Sem sliders";
                            
                            endif;
                                


                        ?>
            </div>
                         

    </div>
    
<?php get_footer(); ?>