<?php get_header(); ?>

        <div class="title-section module">
            <div class="row">
        
                <div class="small-12 columns">
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
        
                <div class="small-12 columns">
                    <ul class="breadcrumbs">
                        <li><a href="#">Home</a></li>
                        <li class="disabled">Serviços</li>
                        <li><span class="show-for-sr"><?php echo get_the_title(); ?></li>
                    </ul>
                </div>
                
            </div>
        </div>
         

 	    <div class="single-service module clearfix" style="margin-bottom:0px !important;">
			<div class="row">

				<div class="medium-6 small-12 columns" style="margin-bottom: 20px;">
                    <?php echo apply_filters('the_content', get_post_field('post_content', get_the_id())); ?>
				</div>
				
				<div class="medium-6 small-12 columns">
                <section class="slider" style="height:auto; margin-top:0px">
                        <div id="slider" class="flexslider" style="margin: 0 0 0px;">     
                            <?php 
                               
                                $images = get_field('galeria');
                                    
                                if( $images ): ?>
                                <ul class="slides">
                                    <?php foreach( $images as $image ): 
                                        ?>
                                    
                                        <li>
                                            <img src="<?php  echo wp_get_attachment_image_src($image['ID'],'tamanho-servicos')[0]; ?>" alt="<?php echo get_the_title(); ?>">
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>

                            </div>
                            <div id="carousel" class="flexslider">
                            <?php 
                                
                                $images = get_field('galeria');
                                $size = 'tamanho-medio';

                                if( $images ): ?>
                                <ul class="slides">
                                    <?php foreach( $images as $image ): ?>
                                        <li>
                                            <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                            </ul>
                        </div>
                    </section>
				</div>

                
				<div class="medium-12 small-12 columns">
                    <?php echo apply_filters('the_content', get_post_field('paragrafo2', get_the_id())); ?>
				</div>
			</div>
        </div>
        
        
        <div class="reason-section module">
            <div class="row">
            	
                <div class="small-12 medium-6 columns">
            		
                <section id="mapa" class="gmap-contact">
                    <div class="gmap-wrapper">
                        <div class="google-map" id="gmap_contact" style="height: 500px;"></div>
                    </div>
                </section>
                
                </div>
                
                <div class="small-12 medium-6 columns">
                
                    <div class="section-title">
            			<h2>Entre em <span>Contato</span></h2>
						<p>Solicite um orçamento agora mesmo! Preencha os campos abaixo e nos envie uma mensagem.</p>
            		</div>
                	
                    <div class="estimate-form row">
                        <form action="#" method="POST"> 

                            <?php 

                                if (isset($_POST['nome'])) {
                                    
                                    $nome = $_POST['nome'];
                                    $sobrenome = $_POST['sobrenome'];
                                    $email = $_POST['email'];
                                    $assunto = $_POST['assunto'];
                                    $mensagem = $_POST['mensagem'];
                             
                                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                                    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

                                    $mensagem = wordwrap('Nome: ' . $nome . ' ' . $sobrenome . '<br/> E-mail' . $email . '<br/> Mensagem: ' . $mensagem. '<br/> Assunto: ' . $assunto, 70);                                    
                                    mail('contato@geoscan.com.br', $assunto, $mensagem, $headers);
                                    echo '<script> alert("Mensagem Enviada com Sucesso") </script>';

                                }    

                            ?>

                            <div class="medium-6 small-12 columns">
                                <label>
                                    <input name="nome" type="text" placeholder="Nome" />
                                </label>
                            </div>
                            
                            <div class="medium-6 small-12 columns">
                                <input name="sobrenome" type="text" placeholder="E-mail" />
                            </div>
                       	
                            <div class="medium-12 small-12 columns">
                                <select name="assunto">
                                <?php

                                    $args = array(
                                        'post_type'         => 'servicos',
                                        'posts_per_page'    => -1,
                                        'order'             => 'ASC',
                                    );

                                    $the_query = new WP_Query( $args );

                                    if ( $the_query->have_posts() )
                                    {
                                        while ( $the_query->have_posts() )
                                        {
                                            $the_query->the_post();
                                
                                ?>

                                        <option value="<?php echo get_post_field('post_name'); ?>"><?php echo get_the_title(); ?></option>

                                <?php
                                    
                                        }
                                        wp_reset_postdata();
                                    }
                                    else
                                    {
                                        echo "Sem serviços cadastrados";
                                    }
                                    
                                ?>

                                </select>
                                
                                <textarea name="mensagem" rows="7" placeholder="Descreva aqui sua necessidade ..."></textarea>
                                <button  type="submit" class="button primary">Enviar</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
                
            </div>
        </div>
    
<?php get_footer(); ?>