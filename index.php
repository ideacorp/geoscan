<?php get_header();?>
        
        
        <div class="main-banner module">
        
            <div style="background:url('<?php echo get_field("imagem_de_fundo"); ?>');" class="slide transparent-background-one">
            	<div class="row">
                	
                    <div class="medium-12 small-12 columns">
                        <div class="banner-text">
                            <h2>Lorem ipsum To <span>Dolor sit Amet,</span> Sed do Eiusmod!</h2>
                            <h3><i class="fa fa-check" aria-hidden="true"></i>Incididunt ut labore et dolore magna
                            <br /><i class="fa fa-check" aria-hidden="true"></i>Incididunt ut labore et dolore magna aliqua</h3>
                            <a href="#" class="button primary">Veja mais</a> <a href="#" class="button secondary">Entre em contato</a>
                        </div>
                        <div class="banner-form">
                            <h4>Solicite um orçamento! </h4>
                            <h6>A melhor equipe<br>Sempre a disposição para lhe entender </h6>
                            <form>                
                            	<input type="text" value="" placeholder="Nome *" />                   
                                <input type="email" value="" placeholder="E-mail *" />
                                <input type="text" value="" placeholder="Telefone *" />                                
                                <textarea id="message" class="form-control" rows="4" placeholder="Escreva sua mensagem *" required></textarea>
                                <button type="submit" class="button primary">Enviar</button>
                    		</form>
            			</div>
                    </div>
                
                </div>
            </div>

        	<div class="slide transparent-background">
            	<div class="row">
                	
                    <div class="medium-12 small-12 columns">
                        <div class="banner-text">
                            <h2>Lorem ipsum  <span>dolor sit amet,</span>  CONSECTETUR ADIPISCING ELIT</h2>
                            <h3><i class="fa fa-check" aria-hidden="true"></i> Sed do eiusmod tempor incididunt 
                            <br /><i class="fa fa-check" aria-hidden="true"></i> Incididunt ut labore et dolore magna aliqua</h3>
                            <a href="#" class="button primary">Veja mais</a> <a href="#" class="button secondary">Entre em contato</a>
                        </div>
                        <div class="banner-image">
                        	<img src="<?php echo get_template_directory_uri()?>/assets/images/bg-man-slider.png" alt="banner-img" style="width: 260px;"/>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
        
        <div class="about-section module">
            <div class="about-section-top">
                <div class="row">
                    
                    <div class="small-12 medium-6 columns section-top">
                        <div class="description-side">
                            <h2>Seja Bem vindo a <span>GeoScan</span></h2>
                            <h4>Nós somos os melhores em...</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                        </div>
                    </div>
                    
                    <div class="medium-6 small-12 columns about-image">
                    	<img src="<?php echo get_template_directory_uri()?>/assets/images/bg-welcome.png" alt="Plumber Introduction" />
                    </div>
                                
                </div>
            </div>
        </div>
        
        
        <div class="about-section-bottom module">
            <div class="row">
            
                <div class="medium-4 small-12 columns">
                    <div class="section-inner">
                        <div class="section-icon">
                            <i class="fa fa-paper-plane" aria-hidden="true"></i>
                        </div>
                        <div class="section-tex">
                            <h2>Qualidade</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
                        </div>
                    </div>
                </div>
                
                <div class="medium-4 small-12 columns section-bottom">
                    <div class="section-inner">
                        <div class="section-icon">
                            <i class="fa fa-wrench" aria-hidden="true"></i>
                        </div>
                        <div class="section-tex">
                            <h2>Custo Benefício</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
                        </div>
                    </div>
                </div>
                
                <div class="medium-4 small-12 columns section-bottom">
                    <div class="section-inner">
                        <div class="section-icon">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                        </div>
                        <div class="section-tex">
                            <h2>Eficiência</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        

        <div class="service-banner">
        	<div class="row relative-position">
            
                <img src="<?php echo get_template_directory_uri()?>/assets/images/bg-man-slider.png" style="width: 260px;" alt="Service Provider Image" />
                
                <div class="medium-9 small-12 columns medium-offset-3 services-box">
                    <h2 style="font-size: 23px;"><i class="fa fa-life-ring" aria-hidden="true"></i> Entre em contato, teremos o maior prazer em atendê-lo!</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                	<div class="call-box"><i class="fa fa-phone"></i>  (85) 4101-9199 &nbsp;&nbsp;&nbsp;<i class="fa fa-envelope"></i> contato@geoscan.com.br</div>
                </div>
            	
            </div>
        </div>
         
        
         <div class="services module grey-bg">
            
            <div class="section-title small-module">
                <h2><span>Nossos Principais</span> Serviços</h2>
                
            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
			
        	<div class="row">
            
                <div class="small-12 medium-4 columns">
                	<div class="service service-type-two">
                    	<div class="service-thumbnail">
                    		<a href="#">
                    			<img src="<?php echo get_template_directory_uri()?>/assets/images/bg-services.png" alt="" />
                    		</a>
						</div>
                        <div class="service-text">
                       		<a href="#"><h2><i class="fa fa-hourglass-start"></i> Serviço 1</h2></a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                
                <div class="small-12 medium-4 columns">
                	<div class="service service-type-two">
                    	<div class="service-thumbnail">
                    		<a href="#">
                    			<img src="<?php echo get_template_directory_uri()?>/assets/images/bg-services.png" alt="" />
                    		</a>
						</div>
                        <div class="service-text">
                       		<a href="#"><h2><i class="fa fa-home"></i> Serviço 2</h2></a>
                        	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                
                <div class="small-12 medium-4 columns">
                	<div class="service service-type-two">
                    	<div class="service-thumbnail">
                    		<a href="#">
                    			<img src="<?php echo get_template_directory_uri()?>/assets/images/bg-services.png" alt="" />
                    		</a>
						</div>
                        <div class="service-text">
                       		<a href="#"><h2><i class="fa fa-thermometer-quarter"></i> Serviço 3</h2></a>
                        	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                
                <div class="small-12 medium-4 columns">
                	<div class="service service-type-two">
                    	<div class="service-thumbnail">
                    		<a href="#">
                    			<img src="<?php echo get_template_directory_uri()?>/assets/images/bg-services.png" alt="" />
                    		</a>
						</div>
                        <div class="service-text">
                       		<a href="#"><h2><i class="fa fa-fire"></i> Serviço 4</h2></a>
                        	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                
                <div class="small-12 medium-4 columns">
                	<div class="service service-type-two">
                    	<div class="service-thumbnail">
                    		<a href="#">
                    			<img src="<?php echo get_template_directory_uri()?>/assets/images/bg-services.png" alt="" />
                    		</a>
						</div>
                        <div class="service-text">
                       		<a href="#"><h2><i class="fa fa-shower" ></i> Serviço 5</h2></a>
                        	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                
                <div class="small-12 medium-4 columns">
                	<div class="service service-type-two">
                    	<div class="service-thumbnail">
                    		<a href="#">
                    			<img src="<?php echo get_template_directory_uri()?>/assets/images/bg-services.png" alt="" />
                    		</a>
						</div>
                        <div class="service-text">
                       		<a href="#"><h2><i class="fa fa-bath"></i> Serviço 6</h2></a>
                        	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
        
        <div class="projects module">
        	
            <div class="section-title small-module">
            	<h2><span>Acompanhe </span>nosso Trabalho</h2>
                <a href="#" class="button primary float-right">Ir para o Instagram</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
            </div>
            
            <div class="row">
            	<div class="projects-wrap small-12 columns">
                	
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                           
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
	                    	<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        	
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	                    	
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	                    	
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	                    	
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	                    	
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	                    	
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	                    	
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	                    	
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	                    	
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	                    	
                        </div>        
                    </div>
                    
                    <div class="project">
                    	<img class="gallery-thumb" src="<?php echo get_template_directory_uri()?>/assets/images/bg-insta.png" alt=""/>
                        <div class="project-detail">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	                    	
                        </div>        
                    </div>
                    
                </div>
            </div>
        </div>
        
        
        <div class="section-stats">
			<div class="row">
            
				<div class="medium-3 small-12 columns">
					<div class="stats-box" >
						<i class="fa fa-unlock" aria-hidden="true" style="color: white; border-color: white;"></i>
						<h2 style="color: white;">Projetos</h2>
						<p>4000+</p>
					</div>
				</div>

				<div class="medium-3 small-12 columns">
					<div class="stats-box">
						<i class="fa fa-users" aria-hidden="true" style="color: white; border-color: white;"></i>
						<h2 style="color: white;">Clientes</h2>
						<p>25000+</p>
					</div>
				</div>
            
				<div class="medium-3 small-12 columns">
					<div class="stats-box">
						<i class="fa fa-trophy" aria-hidden="true" style="color: white; border-color: white;"></i>
						<h2 style="color: white;">Prêmios</h2>
						<p>1000+</p>
					</div>
				</div>
            
				<div class="medium-3 small-12 columns">
					<div class="stats-box">
						<i class="fa fa-thumbs-o-up" aria-hidden="true" style="color: white; border-color: white;"></i>
						<h2 style="color: white;">Likes</h2>
						<p>50000+</p>
					</div>
				</div>
                
            </div>
        </div>
        
     	
        
        <div class="why-choose-us module">
            <div class="why-choose-us-border">
            	
            	<div class="section-title small-module">
            		<h2><span>Por que </span>nos escolher?</h2>
                	<p>Sed ut perspiciatis unde omnis iste </p>
            	</div>
            	
            	<div class="row">
                	<div class="small-12 medium-6 columns">
                	
                    	<div class="why-us-wrapper">
                    		<h1>1</h1>
                    		<h2>Motivo.</h2>
                        	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy. </p>
                    	</div>
                	
                    	<div class="why-us-wrapper">
                    		<h1>2</h1>
                    		<h2>Motivo.</h2>
                        	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy. </p>
                    	</div>
                    
                    	<div class="why-us-wrapper">
                    		<h1>3</h1>
                    		<h2>Motivo.</h2>
                        	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy. </p>
                    	</div>
                    
                	</div>
            
            	</div>
            </div>
        </div>    
        
        
        <div class="Latest-News module">
        	
            <div class="section-title">
        		<h2><span>Acompanhe </span>nosso Blog</h2>
        		<a href="#" class="button primary float-right">Visite nosso blog</a>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
        	</div>
        
        	<div class="row">
        
        		<div class="medium-4 small-12 columns">
                    <div class="news">
                        <a href="#">
	                        <img src="<?php echo get_template_directory_uri()?>/assets/images/bg-blog.png" alt="" class="thumbnail" />
						</a>    
                        <div class="news-text">
                    		<h2><a href="#">Novidade 1</a></h2>
                    		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. <a href="#">Leia mais →</a></p>
                    	</div>
                    	<div class="news-date">
                            <h2>Junho / 24</h2>
                            <p>por <a href="#">Nome</a><span>/</span>6 <a href="#">Comentários</a></p>
                        </div>
                    </div>
        		</div>
                
                <div class="medium-4 small-12 columns">
                    <div class="news">
                        <a href="#">
                        	<img src="<?php echo get_template_directory_uri()?>/assets/images/bg-blog.png" alt="" class="thumbnail" />
						</a>
                        <div class="news-text">
                    		<h2><a href="#">Novidade 2</a></h2>
                    		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. <a href="#">Leia mais →</a></p>
                    	</div>
                    	<div class="news-date">
                            <h2>Maio / 24</h2>
                            <p>por <a href="#">Nome</a><span>/</span>6 <a href="#">Comentários</a></p>
                        </div>
                    </div>
        		</div>
                
                <div class="medium-4 small-12 columns">
                    <div class="news">
                       	<a href="#">
                       		<img src="<?php echo get_template_directory_uri()?>/assets/images/bg-blog.png" alt="" class="thumbnail" />
						</a>
                        <div class="news-text">
                    		<h2><a href="#">Novidade 3</a></h2>
                    		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. <a href="#">Leia mais →</a></p>
                    	</div>
                    	<div class="news-date">
                            <h2>Agosto / 24</h2>
                            <p>por <a href="#">Nome</a><span>/</span>6 <a href="#">Comentários</a></p>
                        </div>
                    </div>
        		</div>
                
        	</div>
        </div>   
   		
        
        
        <div class="testimonials dark-bg">
        	
            <div class="section-title">
            	<h2>Comentários</h2>
				<p>Estamos sempre buscando fazer nossos clientes felizes!</p>
            </div>
        	
            <div class="testimonials-box">
      			
                <div class="slide">
            	    <div class="row">
						
                        <div class="medium-6 small-12 columns">
                            <div class="testimonials-section">
                                <div class="title">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/bg-comentario.png" alt=""/>
                                    <span class="name">Cliente 1</span>
                                    <span class="designation">Cargo 1</span>
                                    <p><span>,,</span><br> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>
						
                        <div class="medium-6 small-12 columns">
                            <div class="testimonials-section">
                                <div class="title">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/bg-comentario.png" alt=""/>
                                    <span class="name">Cliente 2</span>
                                    <span class="designation">Cargo 2</span>
                                    <p><span>,,</span><br> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>
					
                    </div>
            	</div>
                
                <div class="slide">
            	    <div class="row">
						
                        <div class="medium-6 small-12 columns">
                            <div class="testimonials-section">
                                <div class="title">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/bg-comentario.png" alt=""/>
                                    <span class="name">Cliente 3</span>
                                    <span class="designation">Cargo 3</span>
                                    <p><span>,,</span><br> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>
						
                        <div class="medium-6 small-12 columns">
                            <div class="testimonials-section">
                                <div class="title">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/bg-comentario.png" alt=""/>
                                    <span class="name">Cliente 4</span>
                                    <span class="designation">Cargo 4</span>
                                    <p><span>,,</span><br> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>
					
                    </div>
            	</div>
                
                <div class="slide">
            	    <div class="row">
						
                        <div class="medium-6 small-12 columns">
                            <div class="testimonials-section">
                                <div class="title">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/bg-comentario.png" alt=""/>
                                    <span class="name">Cliente 5</span>
                                    <span class="designation">Cargo 5</span>
                                    <p><span>,,</span><br> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>
						
                        <div class="medium-6 small-12 columns">
                            <div class="testimonials-section">
                                <div class="title">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/bg-comentario.png" alt=""/>
                                    <span class="name">Cliente 6</span>
                                    <span class="designation">Cargo 6</span>
                                    <p><span>,,</span><br> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>
					
                    </div>
            	</div>
                
            </div>
        </div>
        
        
        <div class="reason-section module grey-bg">
            <div class="row">
            	
                <div class="small-12 medium-6 columns">
            		
                    <div class="section-title">
            			<h2><span>Perguntas  </span>Frequentes</h2>
						<p>Algumas das dúvidas mais frequentes de nossos clientes.</p>
            		</div>
                    
                    <ul class="accordion" data-accordion data-deep-link="true" data-update-history="true" data-deep-link-smudge="500" id="deeplinked-accordion">
                        <li class="accordion-item is-active" data-accordion-item>
                            <a href="#" class="accordion-title">Pergunta 1?</a>
                            <div class="accordion-content" data-tab-content id="deeplink1">
                               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.
                            </div>
                        </li>
                        <li class="accordion-item" data-accordion-item>
                            <a href="#" class="accordion-title">Pergunta 2?</a>
                            <div class="accordion-content" data-tab-content id="deeplink2">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries. 
                            </div>
                        </li>
                        <li class="accordion-item" data-accordion-item>
                            <a href="#" class="accordion-title">Pergunta 3</a>
                            <div class="accordion-content" data-tab-content id="deeplink3">
                            	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries. 
                            </div>
                        </li>
                        <li class="accordion-item" data-accordion-item>
                            <a href="#" class="accordion-title">Pergunta 4?</a>
                            <div class="accordion-content" data-tab-content id="deeplink4">
                            	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries. 
                            </div>
                        </li>
                    </ul>
                
                </div>
                
                <div class="small-12 medium-6 columns">
                
                    <div class="section-title">
            			<h2><span>Entre em </span>Contato</h2>
						<p>Solicite um orçamento agora mesmo! Preencha os campos abaixo e nos envie uma mensagem.</p>
            		</div>
                	
                    <div class="estimate-form row">
						<div class="medium-6 small-12 columns">
							<label>
								<input type="text" placeholder="Nome ..." />
							</label>
						</div>
						<div class="medium-6 small-12 columns">
							<input type="text" placeholder="E-mail  ..." />
						</div>
                       	<div class="medium-12 small-12 columns">
                        	<select>
                            	<option value="Serviço 1">Serviço 1</option>
								<option value="Serviço 2">Serviço 2</option>
								<option value="Serviço 3">Serviço 3</option>
                            	<option value="Serviço 4">Serviço 4</option>
                            	<option value="Serviço 5">Serviço 5</option>
							</select>
                        	<textarea rows="7" placeholder="Descreva aqui sua necessidade ..."></textarea>
                    		<button  type="submit" class="button primary">Enviar</button>
						</div>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
        
        <div class="our-brands module">
        	
            <div class="section-title">
                <h2><span>Nossos </span>Parceiros</h2>
                <p>Confira abaixo as parcerias que fechamos ao longo de toda nossa história.</p>                
            </div>
            
            <div class="row">
        
        		<div class="brands-wrapper">                         
        			<div class="brand-logo">
            			<a href="#">
            				<img  alt="" src="<?php echo get_template_directory_uri()?>/assets/images/bg-parceiro.png" class="thumbnail" />
            			</a>
        			</div>
        			<div class="brand-logo">
            			<a href="#">
            				<img  alt="" src="<?php echo get_template_directory_uri()?>/assets/images/bg-parceiro.png" class="thumbnail" />
            			</a>
        			</div>
        			<div class="brand-logo">
            			<a href="#">
            				<img  alt="" src="<?php echo get_template_directory_uri()?>/assets/images/bg-parceiro.png" class="thumbnail" />
            			</a>
        			</div>
                    <div class="brand-logo">
                        <a href="#">
                        	<img  alt="" src="<?php echo get_template_directory_uri()?>/assets/images/bg-parceiro.png" class="thumbnail" />
                        </a>
                    </div>
                    <div class="brand-logo">
                        <a href="#">
                        	<img  alt="" src="<?php echo get_template_directory_uri()?>/assets/images/bg-parceiro.png" class="thumbnail" />
                        </a> 
                    </div>                 
                </div>
        
        	</div>
        </div>
        
       <?php get_footer();?>