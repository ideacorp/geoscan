<!doctype html>
<html lang="pt-br">
<head>

    <?php wp_head(); ?>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo get_bloginfo('name') ?> - <?php echo get_bloginfo('description') ?> - <?php echo get_the_title(); ?></title>
    <meta name="author" content="Ideacorp">
    <meta name="keywords" content="">
    <meta name="msvalidate.01" content="E63ED06D7C0966F4742547C9F41F1F94" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script>
       window.onload = function() {
           document.body.className += " loaded";
           document.getElementById("loaderpre").innerHTML = "";
           document.getElementById("loaderpre").style.display = "none";
       };  

    </script>

    <style>        
        /*-------------------------
            22 - Page Preloader  
        -------------------------*/
        .preloader {
            width:100%;
            height:100%;
            position:fixed;
            top:0px;
            z-index:99999999999;
            background-color:#f7f7f7; 
        }
        .spinner {
            width: 100px;
            height: 100px;
            margin:auto; 
            position:absolute;
            top:50%;
            left:50%;
            margin-top:-50px;
            margin-left:-50px;
        }
        .double-bounce1, .double-bounce2 {
            width: 100%;
            height: 100%;
            border-radius: 50%;
            background-color: #24707e;
            opacity: 0.6;
            position: absolute;
            top: 0;
            left: 0;
            -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
            animation: sk-bounce 2.0s infinite ease-in-out;
        }
        .double-bounce2 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
        }
        @-webkit-keyframes sk-bounce {
            0%, 100% { -webkit-transform: scale(0.0) }
            50% { -webkit-transform: scale(1.0) }
        }
        @keyframes sk-bounce {
            0%, 100% { 
                transform: scale(0.0);
                -webkit-transform: scale(0.0);
            } 50% { 
                transform: scale(1.0);
                -webkit-transform: scale(1.0);
            }
        }
        /*-------------------------
            Page Preloader Ends  
        -------------------------*/
    </style>

    


</head>

<body style="background-color:#ffffff">

	<div class="main-container boxed">
    
        <div class="topBar">
        	<div class="row">
            
            	<div class="large-3 medium-3 small-12 columns call-icon">
                    <p><i class="fa fa-phone" aria-hidden="true"></i>
                        <?php echo get_field('dados_de_contato', 'option')['telefone'] ?>
                    </p>
                </div>
                
                <div class="large-6 medium-5 small-12 columns border menu-centered bg-color">
                    <ul class="menu">
                        <li><a href="<?php echo get_field('dados_de_contato', 'option')['facebook'] ?>"  target="_blank" ><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="<?php echo get_field('dados_de_contato', 'option')['instagram'] ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a href="<?php echo get_field('dados_de_contato', 'option')['linkedin'] ?>"  target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                
                <div class="large-3 medium-4 small-12 columns right-topBar text-right">
                    <a href="http://www.geoscan.com.br/contato/"><i class="fa fa-envelope" aria-hidden="true"></i><?php echo get_field('dados_de_contato', 'option')['contato'] ?></a>
                </div>
            
            </div>
        </div>
        
        <div class="header" style="padding-top: 15px; padding-bottom: 15px;">
        	<div class="row">
            
            	<div class="large-3 medium-12 small-12 columns">
                	<div class="logo">
                    	<a href="http://www.geoscan.com.br/">
	                    	<img src="<?php echo get_template_directory_uri()?>/assets/images/logo.png" alt="logo.site /" style="width: 100%; height: auto;">
                        </a>    
                    </div>
                </div>
                
                <div class="large-9 medium-12 small-12 columns nav-wrap" style="padding-top: 25px;">
                
                	
                    <div class="top-bar">
						<div class="top-bar-title">
							<span data-responsive-toggle="responsive-menu" data-hide-for="large">
								<a data-toggle><span class="menu-icon dark float-left"></span></a>
							</span>
						</div>
                      
                        <nav id="responsive-menu">
                            <ul class="menu vertical large-horizontal float-right" data-responsive-menu="accordion large-dropdown">

                            <?php
                                    
                                $localizacao = get_nav_menu_locations();
                                $menu = wp_get_nav_menu_object( $localizacao[ 'menu-principal' ] );
                                $menuitems = wp_get_nav_menu_items( $menu->term_id);
                                
                                $aberto = False;
                                $saida = "";
                                $contador = 0;

                                foreach( $menuitems as $item )
                                {
                                    $link = $item->url; 
                                    $titulo = $item->title;
                                    $class = $item->classes[0];

                                    if ( $item->menu_item_parent == 0 )
                                    {
                                        if ($menuitems[$contador+1]->menu_item_parent == 0)
                                        {
                                            $saida .= ' <li class="single-sub parent-nav letra-maiuscula">
                                                            <a href="'.$link.'">'.$titulo.'</a> 
                                                        </li>';
                                        }
                                        else
                                        {
                                            $saida .= ' <li class="single-sub parent-nav letra-maiuscula">
                                                            <a href="'.$link.'">'.$titulo.'</a>
                                                            <ul class="child-nav menu vertical">';

                                            $aberto = True;
                                        }   
                                    }
                                    else
                                    {
                                        if ($menuitems[$contador+1]->menu_item_parent == 0)
                                        {
                                            $saida .=  '        <li>
                                                                    <a href="'.$link.'">'.$titulo.'</a>
                                                                </li>
                                                            </ul>
                                                        </li>';
                                        }
                                        else
                                        {
                                            $saida .= ' <li>
                                                            <a href="'.$link.'">'.$titulo.'</a> 
                                                        </li>';
                                        }
                                    }
                                    
                                    $contador++;
                                }
                                echo $saida;

                            ?> 
                            
                            </ul>
                        </nav>
                    </div>
                    
                    
                    <div class="search-wrap float-right">
    					
						<a href="#" class="search-icon-toggle" data-toggle="search-dropdown" style="padding-top:15px">
							<i class="fa fa-search"></i>
						</a>
                    </div>
                    
                    <div class="search-dropdown dropdown-pane" id="search-dropdown" data-dropdown data-auto-focus="true">
                    	<input type="text" placeholder="Digite aqui sua busca e tecle enter ... " />
						<button class="primary button"><i class="fa fa-search"></i></button>
                    </div>
                
                </div>
                
            </div>
        </div>
        