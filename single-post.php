<?php get_header(); ?>
        
	
	<?php 
		update_field('contagem', get_field('contagem')+1);
	 ?>
        <div class="title-section module">
            <div class="row">
        
                <div class="small-12 columns">
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
        
                <div class="small-12 columns">
                    <ul class="breadcrumbs">
                        <li><a href="#">Home</a></li>
                        <li class="disabled">Blog</li>
                        <li><span class="show-for-sr">Current: </span> Postagem</li>
                    </ul>
                </div>
                
            </div>
        </div>

 	    <div class="single-post module">
			<div class="row">
				
				<div class="medium-9 small-12 columns">
					<div class="row">
					
						<div class="medium-12 small-12 columns">
							<div class="news">
								<img src="<?php echo wp_get_attachment_image_src(get_field('imagem'),'imagem-sobre')[0]; ?>" alt="" class="thumbnail" />
								<div class="news-text">
									<?php echo apply_filters('the_content', get_post_field('post_content', get_the_id())); ?>
								</div>
								<div class="news-date">
									<h2><?php echo get_the_date(); ?></h2>
								</div>
							</div>
							
							<div class="clearfix"></div>
						
							<div class="sharing-posts">
								<div class="medium-10 small-12 columns">
									<div class="post-tags">
										<ul class="tags">

										<?php
                
											$tags = get_the_terms(get_the_id(),'post_tag');
											
											if ($tags) 
											{
												foreach ( $tags as $tag ) 
												{
													?>
														<li><a href="<?php echo get_term_link($tag->term_id) ?>"><?php echo $tag->name ?></a></li>
													<?php
												}
											}
											
										?>

										</ul>
									</div>
								</div>   

								<div class="medium-2 small-12 columns">
									<div class="post-share">
										<a href="#"><i class="fa fa-facebook"></i></a>
										<a href="#"><i class="fa fa-twitter"></i></a>
										<a href="#"><i class="fa fa-google"></i></a>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
                    
							<div class="medium-12 columns commentbox row">
								<div class="row">
								
								<?php 
								
										$campos =  array
										(
											'author' 			=> '<div class="medium-6 columns"><input id="author" placeholder="Nome" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .'" size="30"' . $aria_req . ' /></div>',
											'email' 			=> '<div class="medium-6 columns"><input id="email" placeholder="Email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .'" size="30"' . $aria_req . ' /></div>',
											'url' 				=> '<div class="medium-12 columns"><input id="url" name="url" placeholder="Website" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .'" size="30" /></div>',
										);


										$comments_args = array(
												
												'id_form'           => '',
												'class_form'      	=> '',
												'id_submit'         => '',
												'class_submit'      => '',
												'name_submit'       => 'enviar',
												'submit_field'		=> '<div class="medium-12 columns"><button class="button primary" type="submit">Enviar comentário</button></div>',
												'title_reply'       => __( 'Responder' ),
												'title_reply_to'    => __( 'Responder para %s' ),
												'cancel_reply_link' => __( 'Cancelar Resposta' ),
												'label_submit'      => __( 'Postar Comentário' ),
												'format'            => 'xhtml',
												'fields' 			=> apply_filters( 'comment_form_default_fields', $campos ),
												'comment_field'     => '<div class="medium-12 columns"></label> <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></div>',
												
										);
									
										comment_form($comments_args);
								
								
								?>
							
								</div>
							</div>
						</div>
						
						
						
					</div>
					<div class="clearfix"></div>
				</div>
				
				<?php get_sidebar(); ?>
				
			</div>
        </div>
        
        
<?php get_footer(); ?>