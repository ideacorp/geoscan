<?php 


// Gerar Menu 

function Menufooter() 
{
    register_nav_menu('menu-footer',__( 'Menu footer' ));
}

add_action( 'init', 'Menufooter' );


function MenuPrincipal() 
{
    register_nav_menu('menu-principal',__( 'Menu Principal' ));
}

add_action( 'init', 'MenuPrincipal' );

// Adicionar suporte a imagens personalizadas

add_image_size( 'imagem-parceiros', 250, 77, true );
add_image_size( 'imagem-chamada-contato', 260, 342, true );
add_image_size( 'imagem-servicos', 329, 184, true );
add_image_size( 'imagem-chamada-blog', 370, 370, true );
add_image_size( 'imagem-sobre', 570, 349, true );
add_image_size( 'imagem-galeria', 570, 300, true );
add_image_size( 'imagem-slide-fundo', 1250, 655, true );
add_image_size( 'tamanho-medio', 360, 200, true );
add_image_size( 'tamanho-servicos', 662, 573, array( 'center', 'center' ));

// Registrar Custom Page Descontos


    // Registrar Custom Page Descontos

function func_depoimentos() {
    
        $labels = array(
            'name'                  => _x( 'Depoimentos', 'Post Type General Name', 'depoimentos' ),
            'singular_name'         => _x( 'Depoimentos', 'Post Type Singular Name', 'depoimentos' ),
            'menu_name'             => __( 'Depoimentos', 'depoimentos' ),
            'name_admin_bar'        => __( 'Depoimentos', 'depoimentos' ),
            'archives'              => __( 'Arquivos', 'depoimentos' ),
            'attributes'            => __( 'Atributos', 'depoimentos' ),
            'parent_item_colon'     => __( 'Parentes', 'depoimentos' ),
            'all_items'             => __( 'Todos', 'depoimentos' ),
            'add_new_item'          => __( 'Adicionar', 'depoimentos' ),
            'add_new'               => __( 'Adicionar', 'depoimentos' ),
            'new_item'              => __( 'Adicionar', 'depoimentos' ),
            'edit_item'             => __( 'Editar', 'depoimentos' ),
            'update_item'           => __( 'Atualizar', 'depoimentos' ),
            'view_item'             => __( 'Visualizar', 'depoimentos' ),
            'view_items'            => __( 'Visualizar', 'depoimentos' ),
            'search_items'          => __( 'Procurar', 'depoimentos' ),
            'not_found'             => __( 'Nada encontrado.', 'depoimentos' ),
            'not_found_in_trash'    => __( 'Nada encontrado', 'depoimentos' ),
            'featured_image'        => __( 'Imagem', 'depoimentos' ),
            'set_featured_image'    => __( 'Definir Imagem', 'depoimentos' ),
            'remove_featured_image' => __( 'Remover Imagem', 'depoimentos' ),
            'use_featured_image'    => __( 'Use o recurso de imagem', 'depoimentos' ),
            'insert_into_item'      => __( 'Inserir item', 'depoimentos' ),
            'uploaded_to_this_item' => __( 'Atualizar esse item', 'depoimentos' ),
            'items_list'            => __( 'Listar Itens', 'depoimentos' ),
            'items_list_navigation' => __( 'Navegar nos itens', 'depoimentos' ),
            'filter_items_list'     => __( 'Filtrar os itens', 'depoimentos' ),
        );
        $rewrite = array(
            'slug'                  => 'depoimentos',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        );
        $args = array(
            'label'                 => __( 'depoimentos', 'depoimentos' ),
            'description'           => __( 'depoimentos', 'depoimentos' ),
            'labels'                => $labels,
            'supports'              => array( ),
            'taxonomies'            => array( 'category' ),
            'hierarchical'          => false,
            'public'                => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-products',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,		
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
            'show_in_rest'          => true,
            'rest_base'             => 'depoimentos-resti',
        );
        register_post_type( 'depoimentos', $args );
    
    }
    add_action( 'init', 'func_depoimentos', 0 );


function func_servicos() {
    
        $labels = array(
            'name'                  => _x( 'Serviços', 'Post Type General Name', 'servicos' ),
            'singular_name'         => _x( 'Serviços', 'Post Type Singular Name', 'servicos' ),
            'menu_name'             => __( 'Serviços', 'servicos' ),
            'name_admin_bar'        => __( 'Serviços', 'servicos' ),
            'archives'              => __( 'Arquivos', 'servicos' ),
            'attributes'            => __( 'Atributos', 'servicos' ),
            'parent_item_colon'     => __( 'Parentes', 'servicos' ),
            'all_items'             => __( 'Todos', 'servicos' ),
            'add_new_item'          => __( 'Adicionar', 'servicos' ),
            'add_new'               => __( 'Adicionar', 'servicos' ),
            'new_item'              => __( 'Adicionar', 'servicos' ),
            'edit_item'             => __( 'Editar', 'servicos' ),
            'update_item'           => __( 'Atualizar', 'servicos' ),
            'view_item'             => __( 'Visualizar', 'servicos' ),
            'view_items'            => __( 'Visualizar', 'servicos' ),
            'search_items'          => __( 'Procurar', 'servicos' ),
            'not_found'             => __( 'Nada encontrado.', 'servicos' ),
            'not_found_in_trash'    => __( 'Nada encontrado', 'servicos' ),
            'featured_image'        => __( 'Imagem', 'servicos' ),
            'set_featured_image'    => __( 'Definir Imagem', 'servicos' ),
            'remove_featured_image' => __( 'Remover Imagem', 'servicos' ),
            'use_featured_image'    => __( 'Use o recurso de imagem', 'servicos' ),
            'insert_into_item'      => __( 'Inserir item', 'servicos' ),
            'uploaded_to_this_item' => __( 'Atualizar esse item', 'servicos' ),
            'items_list'            => __( 'Listar Itens', 'servicos' ),
            'items_list_navigation' => __( 'Navegar nos itens', 'servicos' ),
            'filter_items_list'     => __( 'Filtrar os itens', 'servicos' ),
        );
        $rewrite = array(
            'slug'                  => 'servicos',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        );
        $args = array(
            'label'                 => __( 'servicos', 'servicos' ),
            'description'           => __( 'servicos', 'servicos' ),
            'labels'                => $labels,
            'supports'              => array( ),
            'taxonomies'            => array( 'category' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-products',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,		
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
            'show_in_rest'          => true,
            'rest_base'             => 'servicos-resti',
        );
        register_post_type( 'servicos', $args );
    
    }
    add_action( 'init', 'func_servicos', 0 );

    if( function_exists('acf_add_options_page') ) {
        
        acf_add_options_page(array(
            'page_title' 	=> 'Configurações do Template',
            'menu_title'	=> 'Editar Tema',
            'menu_slug' 	=> 'editar-tema',
            'capability'	=> 'edit_posts',
            'redirect'		=> false
        ));
        
    }

?>