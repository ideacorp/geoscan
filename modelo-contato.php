<?php 

/* Template Name: Contato */

get_header();

?>

        <div class="title-section module">
            <div class="row">
        
                <div class="small-12 columns">
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
        
                <div class="small-12 columns">
                    <ul class="breadcrumbs">
                        <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                        <li><span class="show-for-sr">Current: </span> <?php echo get_the_title(); ?></li>
                    </ul>
                </div>
                
            </div>
        </div>
         
        <div class="appointment-page module">
			<div class="row">
			
				<div class="medium-8 small-12 columns">

                    <section id="mapa" class="gmap-contact">
                        <div class="gmap-wrapper">
                            <div class="google-map" id="gmap_contact" style="height: 400px;"></div>
                        </div>
                    </section>

					<div class="appointment-form">
						
                        <h3 style="padding-top: 20px">Preencha o formulário abaixo e nos envie uma mensagem</h3>
						<p>Entraremos em contato o mais breve possível com a resposta de sua solicitação!</p>
						
                        <form action="#" method="POST"> 

                            <?php 

                                if (isset($_POST['nome'])) {
                                    
                                    $nome = $_POST['nome'];
                                    $sobrenome = $_POST['sobrenome'];
                                    $email = $_POST['email'];
                                    $assunto = $_POST['assunto'];
                                    $mensagem = $_POST['mensagem'];

                                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                                    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

                                    $mensagem = wordwrap('Nome: ' . $nome . ' ' . $sobrenome . '<br/> E-mail' . $email . '<br/> Mensagem: ' . $mensagem. '<br/> Assunto: ' . $assunto, 70);
                                    mail('contato@geoscan.com.br', $assunto, $mensagem, $headers);
                                    echo '<script> alert("Mensagem Enviada com Sucesso") </script>';

                                }    

                            ?>

                            <div class="row">
                                <div class="medium-6 small-12 columns">
                                    <input name="nome" type="text" placeholder="Primeiro nome ..." />
                                </div>
                                <div class="medium-6 small-12 columns">
                                    <input name="sobrenome" type="text" placeholder="Último nome ..." />
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="medium-6 small-12 columns">
                                    <input name="telefone" type="text" placeholder="Telefone ..." />
                                </div>
                                <div class="medium-6 small-12 columns">
                                    <input name="email" type="text" placeholder="Email  ..." />
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="medium-12 small-12 columns">
                                    <input name="assunto" type="text" placeholder="Assunto ..." />
                                </div>
                                <div class="medium-12 small-12 columns">
                                    <textarea name="mensagem" rows="3" placeholder="Mensagem ..."></textarea>
                                </div>
                                <div class="medium-12 small-12 columns">
                                    <input type="submit" class="button primary" value="Enviar!" />
                                </div>
                            </div>
                        
                        </form>
					
					</div>
				</div>
				
				<div class="medium-4 small-12 columns sidebar">
					
					<div class="contact-card">
						<ul>
							<li>
								<i class="fa fa-phone"></i><span><?php echo get_field('dados_de_contato', 'option')['telefone'] ?></span>
							</li>
							<li>
								<i class="fa fa-envelope"></i><span><?php echo get_field('dados_de_contato', 'option')['contato'] ?></span>
							</li>
							<li>
								<i class="fa fa-clock-o"></i><span><?php echo get_field('dados_de_contato', 'option')['horario_de_atendimento'] ?></span>
							</li>
							<li>
								<i class="fa fa-map-marker"></i><span><?php echo get_field('dados_de_contato', 'option')['endereco'] ?></span>
							</li>
						</ul>
					</div>
					
					<div class="contact-card no-bg">
						<ul>
							<li>
								<a href="<?php echo get_field('dados_de_contato', 'option')['facebook'] ?>" target="_blank"><i class="fa fa-facebook"></i></a>
								<a href="<?php echo get_field('dados_de_contato', 'option')['linkedin'] ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
								<a href="<?php echo get_field('dados_de_contato', 'option')['instagram'] ?>" target="_blank"><i class="fa fa-instagram"></i></a>
							</li>
						</ul>
					</div>
					
				</div>
				
			</div>
        </div>    

<?php get_footer(); ?>