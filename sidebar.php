<div class="services-sidebar sidebar medium-3 small-12 columns">
    
    <?php echo get_search_form(); ?>    

    <div class="widget">
        <h2>Categorias do Blog</h2>
        
        <div class="widget-content">
            <ul class="menu vertical">
            
                <?php

                    $categorias = get_categories();
                    if ($categorias) {
                        foreach ( $categorias as $categoria ) 
                        { 
                            echo '<li><a href="'.esc_url( get_category_link( $categoria->term_id ) ).'">'.$categoria->name.'</a></li>';
                        }
                    }
                    else
                    {
                        echo "Não existem categorias cadastradas";
                    }
                
                ?>

            </ul>
        </div> 
    </div>
    
    <div class="widget">
        <h2>Tags Populares</h2>
        
        <div class="widget-content">
            <ul class="tags">
                
                <?php
                
                    $tags = get_tags();
                    $html = '';
                    
                    foreach ( $tags as $tag ) {
                        $tag_link = get_tag_link( $tag->term_id );        
                        $html .= "<li><a href='{$tag_link}' title='{$tag->name}'>";
                        $html .= "{$tag->name}</a></li>";
                    }
                    
                    echo $html;
                ?>
        
            </ul>
        </div>
    <div class="clearfix"></div>
    </div>
    
    <div class="widget">
        <h2>Posts Populares</h2>
        
        <div class="widget-content">
            
            <?php

                $args = array(
                    'posts_per_page'    => 3,
                    'meta_key'	        => 'contagem',
                    'orderby'	        => 'meta_value_num',
                    'order'          => 'ASC',

                );

                $the_query = new WP_Query( $args );

                if ( $the_query->have_posts() )
                {
                    while ( $the_query->have_posts() )
                    {
                        $the_query->the_post();
            ?>
            
                        <div class="popular-post">
                            <a href="<?php echo get_the_permalink(); ?>"><strong><?php echo get_the_title(); ?></strong></a>
                            <p><img style="width: 100%;" alt="<?php echo get_the_title(); ?>" src="<?php echo wp_get_attachment_image_src(get_field('imagem'),'imagem-chamada-contato')[0]; ?>" class="float-left" /><?php echo get_field('paragrafo'); ?></p>
                        </div>
                                        
            <?php
                    }
                    wp_reset_postdata();
                }
                else
                {
                    echo "Sem descontos cadastrados";
                }
            ?>

        </div>
    </div>    

</div>