<?php 

/* Template Name: Página Inicial */

get_header();

?>
        <div class="main-banner module">
        
        <?php

        $z = 15;
            if( have_rows('slider') ):
                
                while ( have_rows('slider') ) : the_row();
                
                
                    if(get_sub_field('orcamento')==1)
                    {
                        ?>

                            <div style="background:url('<?php echo wp_get_attachment_image_src(get_sub_field('imagem_de_fundo'),'imagem-slide-fundo')[0]; ?>')" class="slide transparent-background-one">
                                <div class="row">
                                    
                                    <div class="medium-12 small-12 columns" >
                                        <div class="banner-text">
                                            <h2><?php echo get_sub_field('titulo');  ?> </h2>
                                            <?php echo get_sub_field('texto');  ?>
                                            <a href="<?php echo get_sub_field('botao_esquerdo_link');  ?>" class="button primary"><?php echo get_sub_field('titulo_botao_esquerdo');  ?></a> <a href="<?php echo get_sub_field('botao_direito_link');?>" class="button secondary"><?php echo get_sub_field('titulo_botao_direito'); ?></a>
                                        </div>
                                        <div class="banner-form">
                                            <h4>Solicite um orçamento! </h4>
                                            <form>                
                                                <input type="text" value="" placeholder="Nome *" />                   
                                                <input type="email" value="" placeholder="E-mail *" />
                                                <input type="text" value="" placeholder="Telefone *" />                                
                                                <textarea id="message" class="form-control" rows="4" placeholder="Escreva sua mensagem *" required></textarea>
                                                <button type="submit" class="button primary">Enviar</button>
                                            </form>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>



                        <?php
                    }
                    else
                    {

                        ?>

                            <div style="background:url('<?php echo wp_get_attachment_image_src(get_sub_field('imagem_de_fundo'),'imagem-slide-fundo')[0]; ?>'); background-size: cover;" class="slide transparent-background">
                                <div class="row">

                                    <div class="medium-12 small-12 columns" style="margin-left: 4%;">
                                        <div class="banner-text">
                                            <h2 style="background: linear-gradient(to right, rgba(97, 97, 97, 0.5), transparent) !important; padding-left: 100px; margin-left: -90px;" class="textos_slides_banner"><?php echo get_sub_field('titulo');  ?></h2>
                                            <?php echo get_sub_field('texto');  ?>
                                            <a href="<?php echo get_sub_field('link_botao_esquerdo'); ?>" class="button primary"><?php echo get_sub_field('titulo_botao_esquerdo');  ?></a> <a href="<?php echo get_sub_field('link_botao_direito'); ?>" class="button secondary"><?php echo get_sub_field('titulo_botao_direito'); ?></a>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>


                        <?php

                    }
                    

                endwhile;

                else :

                    echo "Sem sliders";

                endif;

        ?>

        </div>

        <div class="about-section module">
            <div class="about-section-top">
                <div class="row">
                    
                    <div class="small-12 medium-6 columns section-top">
                        <div class="description-side">
                            <?php echo apply_filters('the_content', get_post_field('post_content', get_page_by_path( 'sobre' ) )); ?>
                        </div>
                    </div>
                    
                    <div class="medium-6 small-12 columns about-image">
                        <img style="border: 0px solid #CCC;" src="<?php echo wp_get_attachment_image_src(get_field('imagem',get_page_by_path( 'sobre' )),'imagem-slide-fundo')[0]; ?>" alt="<?php echo get_the_title(get_page_by_path( 'sobre' )); ?>" />
                    </div>
                                
                </div>
            </div>
        </div>

        <div class="about-section-bottom module">
            <div class="row">
            
            <?php

                if( have_rows('diferenciais',get_page_by_path( 'sobre' )) ):

                    while ( have_rows('diferenciais',get_page_by_path( 'sobre' )) ) : the_row();
                        
                        ?>

                            <div class="medium-4 small-12 columns">
                                <div class="section-inner">
                                    <div class="section-icon">
                                        <i class="fa <?php echo get_sub_field('icone',get_page_by_path( 'sobre' )); ?>" aria-hidden="true"></i>
                                    </div>
                                    <div class="section-tex">
                                        <h2><?php echo get_sub_field('titulo',get_page_by_path( 'sobre' )); ?></h2>
                                        <p><?php echo get_sub_field('texto',get_page_by_path( 'sobre' )); ?></p>
                                    </div>
                                </div>
                            </div>

                        <?php
                    
                        
                    endwhile;

                else :

                    echo 'Sem diferenciais cadastrados';

                endif;

            ?>
                
            </div>
        </div>
        
         <div class="services module grey-bg">
            
            <div class="section-title small-module">
                <h2>Nossos <span>Principais Serviços</span></h2>
            	<p>Na GEOSCAN você encontra os melhores serviços nos segmentos de Águas Subterrâneas, Geologia e Geofísica.</p>
            </div>
			
        	<div class="row">

            <?php

                $hoje = date('Ymd');

                $args = array(
                    'posts_per_page'    => 6,
                    'post_type'         => 'servicos',
                    'order'          => 'ASC',
                );

                $the_query = new WP_Query( $args );

                if ( $the_query->have_posts() )
                {
                    while ( $the_query->have_posts() )
                    {
                        $the_query->the_post();
            ?>
            
                                <div class="small-12 medium-4 columns">
                                    <div class="service service-type-two">
                                        <div class="service-thumbnail">
                                            <a href="<?php echo get_the_permalink(); ?>">
                                                <img src="<?php echo wp_get_attachment_image_src(get_field('imagem'),'imagem-servicos')[0]; ?>" alt="" />
                                            </a>
                                        </div>
                                        <div class="service-text">
                                            <a href="<?php echo get_the_permalink(); ?>"><h2><i class="<?php echo get_field('icone'); ?>"></i> <?php echo get_field('titulo_curto'); ?></h2></a>
                                            <p><?php echo get_field('paragrafo'); ?></p>
                                        </div>
                                    </div>
                                </div>
                                        
            <?php
                    }
                    wp_reset_postdata();
                }
                else
                {
                    echo "Sem descontos cadastrados";
                }
            ?>
            
            </div>
        </div>
        
        
        <div class="projects module">
        	
            <div class="section-title small-module">
            	<h2>Acompanhe <span>nosso Trabalho</span></h2>
                <a href="https://www.instagram.com/contatogeoscan/" class="button primary float-right">Ir para o Instagram</a>
                <p>Acompanhe no Instagram nossos últimos projetos!</p>
            </div>
            
            <div class="row">
            	<div class="projects-wrap small-12 columns">
                	
                <?php

                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api.instagram.com/v1/users/self/media/recent/?access_token=6083411708.1677ed0.f7b65da99d2a4f8eaaa7aaa436881e50",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                    ));

                    $response = curl_exec($curl);
                    curl_close($curl);

                    $obj = json_decode($response);
                    $obj = $obj->data;

                    foreach ($obj as $item)
                    {

                        $subteim = $item;

                        $link = $item->link;
                        $comentarios = $item->comments;
                        $tags = $item->tags;
                        $likes = $item->likes->count;

                        $images_alta = $item->images->standard_resolution->url;
                        $images_alta_largura = $item->images->standard_resolution->width;
                        $images_alta_altura = $item->images->standard_resolution->height;

                        $images_media = $item->images->low_resolution->url;
                        $images_media_largura = $item->images->low_resolution->width;
                        $images_media_altura = $item->images->low_resolution->height;

                        $images_pequenas = $item->images->thumbnail->url;
                        $images_alta_largura = $item->images->thumbnail->width;
                        $images_alta_altura = $item->images->thumbnail->height;

                        $texto = $item->caption->text;
                        $data = $item->caption->created_time;

                        ?>
                        <a href="<?php echo $link;?>" target="blank">
                            <div class="project" >      
                               <div class="project" style="height: 100%; width: 100%; background-image: url('<?php echo $images_alta ?>'); background-position: center; background-size: 150%; background-repeat: no-repeat">
                                <!--<img class="gallery-thumb" src="<?php //echo $images_alta ?>" alt=""/>-->
                                <div class="project-detail">
                                    <p><?php echo substr($texto, 0, 140);  ?>...</p>
                                </div>
                                </div>        
                            </div>
                        </a>

                        <?php 
                    }

                    ?>
                    
                
                    
                </div>
            </div>
        </div>
          
        <div class="why-choose-us module" style="background:url('<?php echo wp_get_attachment_image_src(get_field('imagem_valores',get_page_by_path('sobre')),'descontos-pequeno')[0]; ?>'); background-size: contain; background-position: center;">
            <div class="why-choose-us-border">
            	
            	<div class="section-title small-module">
            		<h2>Por que <span>nos escolher?</span></h2>
                	<p>Conheça os principais valores que nos movem! </p>
            	</div>
            	
            	<div class="row">
                	<div class="small-12 medium-6 columns">
                	
                    	<?php

                            $contador = 1;
                            if( have_rows('valores',get_page_by_path('sobre')) ):

                                while ( have_rows('valores',get_page_by_path('sobre')) ) : the_row();
                                    
                                    ?>

                                        <div class="why-us-wrapper">
                                            <h1><?php echo $contador; ?></h1>
                                            <h2><?php echo get_sub_field('titulo'); ?></h2>
                                            <p><?php echo get_sub_field('texto'); ?></p>
                                            <?php $contador++; ?>
                                        </div>

                                    <?php
                            
                                endwhile;

                            else :

                                echo 'Sem diferenciais cadastrados';

                            endif;

                        ?>
                    
                	</div>
            
            	</div>
            </div>
        </div>    
        
        
        <div class="Latest-News module">
        	
            <div class="section-title">
        		<h2>Acompanhe nosso <span>Blog</span></h2>
        		<a href="<?php echo get_site_url(). '/blog/'; ?>" class="button primary float-right">Visite nosso blog</a>
				<p>Se mantenha atualizado sobre as ultimas notícias do nosso segmento!</p>
        	</div>
        
        	<div class="row">
        
            <?php

                $args = array(
                    'posts_per_page'    => 3,
                    'order'          => 'ASC',
                );

                $the_query = new WP_Query( $args );

                if ( $the_query->have_posts() )
                {
                    while ( $the_query->have_posts() )
                    {
                        $the_query->the_post();
            
            ?>

                        <div class="medium-4 small-12 columns">
                            <div class="news">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <img src="<?php echo wp_get_attachment_image_src(get_field('imagem'),'imagem-chamada-blog')[0]; ?>" alt="" class="thumbnail" />
                                </a>    
                                <div class="news-text">
                                    <h2><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
                                    <p><?php echo get_field('paragrafo'); ?> <a href="<?php echo get_the_permalink(); ?>">Leia mais →</a></p>
                                </div>
                                <div class="news-date">
                                    <h2><?php echo get_the_date(); ?></h2>
                                </div>
                            </div>
                        </div>

            <?php
                
                    }
                    wp_reset_postdata();
                }
                else
                {
                    echo "Sem postagens cadastrados";
                }
                
            ?>
                
        	</div>
        </div>   
   		
        <!--
        
        <div class="testimonials dark-bg">
        	
            <div class="section-title">
            	<h2>Comentários</h2>
				<p>Estamos sempre buscando fazer nossos clientes felizes!</p>
            </div>
        	
            <div class="testimonials-box">
        
                <?php
                /*
                    $args = array(
                        'posts_per_page'    => 6,
                        'post_type'         => 'depoimentos',
                        'order'             => 'ASC',
                    );

                    $chave = False;

                    $the_query = new WP_Query( $args );

                    if ( $the_query->have_posts() )
                    {
                        while ( $the_query->have_posts() )
                        {
                            $the_query->the_post();
                
                            if ($chave == False)
                            {

                ?>

                        

                            <div class="slide">
                                <div class="row">    
                                    <div class="medium-6 small-12 columns">
                                        <div class="testimonials-section">
                                            <div class="title">
                                                <img src="<?php echo get_template_directory_uri()?>/assets/images/bg-comentario.png" alt=""/>
                                                <span class="name"><?php echo get_the_title(); ?></span>
                                                <span class="designation"><?php echo get_field('cargo'); ?></span>
                                                <p><span>,,</span><br><?php echo get_field('texto'); ?></p>
                                            </div>
                                        </div>
                                    </div>
                            <?php 

                                $chave = True;
                            }
                            else
                            {

                                ?>

                                            <div class="medium-6 small-12 columns">
                                                <div class="testimonials-section">
                                                    <div class="title">
                                                        <img src="<?php echo get_template_directory_uri()?>/assets/images/bg-comentario.png" alt=""/>
                                                        <span class="name"><?php echo get_the_title(); ?></span>
                                                        <span class="designation"><?php echo get_field('cargo'); ?></span>
                                                        <p><span>,,</span><br><?php echo get_field('texto'); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php 

                                $chave = False;

                            }

                            ?>
                                    
                <?php
                    
                        }
                        wp_reset_postdata();
                    }
                    else
                    {
                        echo "Sem postagens cadastrados";
                    }
                    */
                ?>
                
            </div>
        </div>-->
        
        
        <div class="reason-section module grey-bg">
            <div class="row">
            	
                <div class="small-12 medium-6 columns"> 

                <section id="mapa" class="gmap-contact">
                        <div class="gmap-wrapper">
                            <div class="google-map" id="gmap_contact" style="height: 500px;"></div>
                        </div>
                    </section>
                
                </div>
                
                <div class="small-12 medium-6 columns">
                
                    <div class="section-title">
            			<h2>Entre em <span>Contato</span></h2>
						<p>Solicite um orçamento agora mesmo! Preencha os campos abaixo e nos envie uma mensagem.</p>
            		</div>
                	
                    <div class="estimate-form row">
                        <form action="#" method="POST"> 

                            <?php 

                                if (isset($_POST['nome'])) {
                                    
                                    $nome = $_POST['nome'];
                                    $sobrenome = $_POST['sobrenome'];
                                    $email = $_POST['email'];
                                    $assunto = $_POST['assunto'];
                                    $mensagem = $_POST['mensagem'];
                                    
                                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                                    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                              
                                    $mensagem = wordwrap('Nome: ' . $nome . ' ' . $sobrenome . '<br/> E-mail: ' . $email . '<br/> Mensagem: ' . $mensagem . '<br/> Assunto: ' . $assunto, 70);
                                    mail('contato@geoscan.com.br', $assunto, $mensagem, $headers);
                                    echo '<script> alert("Mensagem Enviada com Sucesso") </script>';

                                }    

                            ?>

                            <div class="medium-6 small-12 columns">
                                <label>
                                    <input name="nome" type="text" placeholder="Nome" />
                                </label>
                            </div>
                            
                            <div class="medium-6 small-12 columns">
                                <input name="email" type="text" placeholder="E-mail" />
                            </div>
                       	
                            <div class="medium-12 small-12 columns">
                                <select name="assunto">
                                <?php

                                    $args = array(
                                        'post_type'         => 'servicos',
                                        'posts_per_page'    => -1,
                                        'order'             => 'ASC',
                                    );

                                    $the_query = new WP_Query( $args );

                                    if ( $the_query->have_posts() )
                                    {
                                        while ( $the_query->have_posts() )
                                        {
                                            $the_query->the_post();
                                
                                ?>

                                        <option value="<?php echo get_post_field('post_name'); ?>" ><?php echo get_the_title(); ?></option>

                                <?php
                                    
                                        }
                                        wp_reset_postdata();
                                    }
                                    else
                                    {
                                        echo "Sem serviços cadastrados";
                                    }
                                    
                                ?>

                                </select>
                                
                                <textarea name="mensagem" rows="7" placeholder="Descreva aqui sua necessidade ..."></textarea>
                                <button  type="submit" class="button primary">Enviar</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
        <div class="our-brands module">
        
            <div class="section-title">
                <h2>Nossos <span>Parceiros e Clientes</span></h2>
                <p>Confira abaixo as parcerias que fechamos ao longo de toda nossa história.</p>                
            </div>

            <div class="partner-banner module">
            
            
            <?php 

                
                if( have_rows('parceiros_e_clientes',get_page_by_path( 'sobre' )) ):
                    
                    while ( have_rows('parceiros_e_clientes',get_page_by_path( 'sobre' )) ) : the_row();

                    ?>
                    

                <div class="brand-logo" style="padding: 25px; display: inline-block; width: 100%; height:160px">
                    <a href="<?php echo get_sub_field('link')  ?> " target="blank">
                    <img src="<?php echo wp_get_attachment_image_src(get_sub_field('imagem'),'imagem-parceiros')[0]; ?>" style="width:90%; display:inline"/> 
                    </a>
                </div>


                <?php

                    endwhile;
                else :
                
                    echo "Sem sliders";
                
                endif;
                    


            ?>

        
            </div>
        
        </div>
        
       <?php get_footer();?>